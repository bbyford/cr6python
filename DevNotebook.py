#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys 
sys.path.insert(1, './PyPak/python/')
import pakbus
import pandas as pd
import numpy as np
import socket
import sys
import pakbus
import time

global IP 
IP = "123.123.123.123"


# In[2]:


# Data logger PakBus Node Id
NodeId = 1
# My PakBus Node Id
MyNodeId = 4094 #pretending to be logger net
# Open socket
#session = pakbus.open_socket(IP,port,Time out)
s = pakbus.open_socket(IP,6785,10)

# check if remote node is up
msg = pakbus.ping_node(s, NodeId, MyNodeId)
if not msg:
    raise Warning('no reply from PakBus node 0x%.3x' % NodeId)
#
# Main program
#
# Get datalogger time difference

#difference in time, what was adjusted = pakbus.clock_sync(session, NodeId, MyNodeId,min_adjust=.1, max_adjust=3)
tdiff, adjust = pakbus.clock_sync(s, NodeId, MyNodeId,min_adjust=.1, max_adjust=3)
import time
print 'PC clock:     ', time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime())
print 'CR1000 clock: ', time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime(time.time() + tdiff)), '(tdiff: %+f seconds)' % tdiff

# say good bye and close socket
pakbus.send(s, pakbus.pkt_bye_cmd(NodeId, MyNodeId))
s.close()


# In[5]:


import sys 
#local copy of datalogger I found some runtime errors that were simple to fix 
#sys.path.insert(1, '/media/icidatabase/ProductionPrograms/PyPak/python/')
sys.path.insert(1, './PyPak/python/')
import pakbus
import pandas as pd
import numpy as np
import calendar
from datetime import datetime

def ntime_to_time(nsec): 
    #datalogger has its own time format this returns time stamp
    # nsec:  NSec value
    nsec_base = calendar.timegm((1990, 1, 1, 0, 0, 0))
    nsec_tick = 1E-9
    # Calculate timestamp with fractional seconds
    timestamp = nsec_base + nsec
    return datetime.fromtimestamp(timestamp)


NodeId = 1
# My PakBus Node Id of the requester 4094 is loggernet the software for fetching data
MyNodeId = 4094
# Open socket
#6785 is defualt 
s = pakbus.open_socket(IP,6785,10)

# check if remote node is up
msg = pakbus.ping_node(s, NodeId, MyNodeId)
if not msg:
    raise Warning('no reply from PakBus node 0x%.3x' % NodeId)

# Get table definition structure
FileData, RespCode = pakbus.fileupload(s, NodeId, MyNodeId, FileName = '.TDF')
if FileData:
    #Tabledef is the format of the table taken from the datalogger
    tabledef = pakbus.parse_tabledef(FileData)
    #Returns data in RecData and a flag if all entries requested were retrieved
    #Table3 is the table defined in the datalogger
    RecData, MoreRecsExist= pakbus.collect_data(s, NodeId, MyNodeId,tabledef,'Table1',                                                P1=30)
# say good bye
pkt = pakbus.pkt_bye_cmd(NodeId, MyNodeId)
pakbus.send(s, pkt)

# close socket
s.close()
count=0
day=pd.DataFrame()
for j in RecData:
    for i in j['RecFrag']:
        stamp=ntime_to_time(i['TimeOfRec'][0])
        daystamp, timestamp=str(stamp).split(' ')
        arrays=np.array([np.array([daystamp]),np.array([timestamp])])
        #db=pd.DataFrame.from_dict({timestamp:i['Fields']},orient='index')
        db=pd.DataFrame(i['Fields'],index=pd.MultiIndex.from_arrays(arrays,names=["Date","Time"]))
        day=day.append(db)


# In[6]:


day


# In[ ]:


import sys 
#local copy of datalogger I found some runtime errors that were simple to fix 
#sys.path.insert(1, '/media/icidatabase/ProductionPrograms/PyPak/python/')
sys.path.insert(1, './PyPak/python/')
import pakbus
import pandas as pd
import numpy as np
import calendar
from datetime import datetime
def ntime_to_time(nsec): 
    #datalogger has its own time format this returns time stamp
    # nsec:  NSec value
    nsec_base = calendar.timegm((1990, 1, 1, 0, 0, 0))
    nsec_tick = 1E-9
    # Calculate timestamp with fractional seconds
    timestamp = nsec_base + nsec
    return datetime.fromtimestamp(timestamp)
def FetchChunk(ChunkNumber):
    # Data logger PakBus Node Id The ID of the the request-y Set on the datalogger
    NodeId = 1
    # My PakBus Node Id of the requester 4094 is loggernet the software for fetching data
    MyNodeId = 4094
    # Open socket
    #6785 is defualt 
    s = pakbus.open_socket(IP,6785,10)

    # check if remote node is up
    msg = pakbus.ping_node(s, NodeId, MyNodeId)
    if not msg:
        raise Warning('no reply from PakBus node 0x%.3x' % NodeId)

    # Get table definition structure
    FileData, RespCode = pakbus.fileupload(s, NodeId, MyNodeId, FileName = '.TDF')
    if FileData:
        #Tabledef is the format of the table taken from the datalogger
        tabledef = pakbus.parse_tabledef(FileData)
        #Returns data in RecData and a flag if all entries requested were retrieved
        #Table3 is the table defined in the datalogger
        RecData, MoreRecsExist= pakbus.collect_data(s, NodeId, MyNodeId,tabledef,'Table1',                                                    P1=ChunkNumber)
    # say good bye
    pkt = pakbus.pkt_bye_cmd(NodeId, MyNodeId)
    pakbus.send(s, pkt)

    # close socket
    s.close()
    count=0
    day=pd.DataFrame()
    for j in RecData:
        for i in j['RecFrag']:
            stamp=ntime_to_time(i['TimeOfRec'][0])
            daystamp, timestamp=str(stamp).split(' ')
            arrays=np.array([np.array([daystamp]),np.array([timestamp])])
            #db=pd.DataFrame.from_dict({timestamp:i['Fields']},orient='index')
            db=pd.DataFrame(i['Fields'],index=pd.MultiIndex.from_arrays(arrays,names=["Date","Time"]))
            day=day.append(db)
    return(day)

def FetchHalfDay():
    ChunkSize=13
    Day=pd.DataFrame()
    for i in range(1,738,ChunkSize):
        Chunk=FetchChunk(i)
        
        Day=pd.concat([Day,Chunk])
        
    return(Day.drop_duplicates())

now=datetime.now()
year=now.year
month=now.month
day=now.day
if now.hour>12:
    hour='PM'
else:
    hour='AM'
testing_things=FetchHalfDay()

